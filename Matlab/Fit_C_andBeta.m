clc
clear all 
close all


%% n and A with constant beta and C

% Q1	Q2	drho	rho2	rho_1ss	hss	n	A
 							
n_matrix=[0	0	101.4	1100	1021	0.1407	-2.2	3489
87	5.6	126	1126	1035	0.1418	-2.02	331.9
0	0	129	1129	1038	0.1786	-2.2	3489
%87	6.5	102	1129	1010	0.1607	-2.2	3489
76	6.5	77.5	1073	1005	0.1614	-2.02	331.9
40	40	54.5	1050	1024	0.1342	-1.5	3.8
40	40	50	1050	1024	0.1342	-1.5	3.8
40	40	14	1010	996	0.145	-1.5	3.8];

col_ind=2;

figure
subplot(121)
plot(n_matrix(:,col_ind),abs(n_matrix(:,7)),'r>','MarkerFaceColor','r')
xlabel('Q2 [ml/min]')
ylabel('abs(n)')
set(gca,'Fontsize',14);

subplot(122)
plot(n_matrix(:,col_ind),n_matrix(:,8),'bo','MarkerFaceColor','b')
xlabel('Q2 [ml/min]')
ylabel('A')
set(gca,'Fontsize',14);


%% C	Q1	Q2	drho	rho2	rho_1ss	hss

C_matrix=[0.45	0	0	101.4	1100	1021	0.1407
0.3	87	5.6	126	1126	1035	0.1418
0.45	0	0	129	1129	1038	0.1786
0.35	76	6.5	77.5	1073	1005	0.1614
0.25	40	40	54.5	1050	1024	0.1342
0.25	40	40	50	1050	1024	0.1342
0.25	40	40	14	1010	996	0.145];

% figure
% plot(C_matrix(:,3),C_matrix(:,1),'bo','MarkerFaceColor','b')
% xlabel('Q2 [ml/min]')
% ylabel('C')
% set(gca,'Fontsize',14);



%% beta	Q1	Q2	drho	rho2	rho_1ss	hss
beta_matrix=[0.06	0	0	101.4	1100	1021	0.1407
0.15	87	5.6	126	1126	1035	0.1418
0.06	0	0	129	1129	1038	0.1786
0.16	76	6.5	77.5	1073	1005	0.1614
0.245	40	40	54.5	1050	1024	0.1342
0.245	40	40	50	1050	1024	0.1342
0.245	40	40	14	1010	996	0.145];
    

%  figure
%  plot(beta_matrix(:,3),beta_matrix(:,1),'bo','MarkerFaceColor','b')
%  xlabel('Q2 [ml/min]')
%  ylabel('\beta')
%  set(gca,'Fontsize',14);
% 


