function [t, h, b1,he,be] = solve_odePumps(w1, w2, b2,b0, h0, K, Tsim,hgrid, beta, n, A)

%  f0 = [h0; b0*h0];

alpha=0;
% alpha=0.1;
he0=alpha*h0;
be0=b0/2;

Htot=0.3;
h20=Htot-(h0+he0);
b20=b2;


f0 = [h0; b0*h0; he0; be0*he0;];% h20; b20*h20];

 Nsteps=1000;
 [t, f] = ode45(@ode, [0:Nsteps:Tsim],f0,[],w1, w2, b2, b0, K, hgrid, beta, n, A);

  h  = f(:, 1);
  b1 = f(:, 2)./f(:, 1);
  he = f(:, 3);
  be = f(:, 4)./f(:, 3);

%   h2 = f(:, 5);
%   b2 = f(:, 6)./f(:, 5);
  
return