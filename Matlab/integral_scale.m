function [Lt,beta]=integral_scale(Q2,z)

% Fit the function of beta=f(Q2)
% Note:Beta has to change to fit all the different Experiments.

% The function is obtained by fitting beta from all the results of our test performed with
% different flow rate Q2

% Beta as a function of Q2 (open Resume_exp excel file on dropbox)
% Q2_conv=Q2*(60/10^-6);
% a=0.099304; b=0.12743; ne=0.24464;  
% beta=a*(Q2_conv+b)^(ne);  


% Hopfinger's constants  (It is more resonable that the thickness of the interface is changing with Ri)
% beta=0.245;  

beta=0.11694;  
% beta=0.245;
%  beta=0.245;

% Assuming linear trend of the integral scale 
Lt=beta*(z);