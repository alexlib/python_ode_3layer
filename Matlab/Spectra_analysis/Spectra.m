clc
clear all
close all

% Create the signal
N=200;
x = linspace(0,10,N);
data = 2*sin(2*3.14159*x);   % f=1 Hz
% noise = rand(1, numElements);
% y = data + noise; 
y=data;
% sampling frequency from the experiment 
fs=40;

% Single-sided magnitude spectrum with frequency axis in Hertz
% Each bin frequency is separated by fs/N Hertz.
X_mags = abs(fft(y));
bin_vals = [0 : N-1];
fax_Hz = bin_vals*fs/N;
N_2 = ceil(N/2);
plot(fax_Hz(1:N_2), X_mags(1:N_2))
xlabel('Frequency (Hz)')
ylabel('Magnitude');
title('Single-sided Magnitude spectrum (Hertz)');
axis tight
 

% [xf,EnrgyDensity,Pyy]= spectrum(x,y);
% 
% figure 
% subplot(121)
% plot(x,y)
% 
% subplot(122)
% plot(xf,EnrgyDensity)
% % plot(xf,Pyy(1:end-1))
% % plot(xf,Pyy(251:500))
% % title('Power spectral density')
% % xlabel('Frequency (Hz)')
