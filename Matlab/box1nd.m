function dydt = box1nd(t, y, B, hg)
d = y(1);
b = y(2);

n = 1.5;
we = d.^(-1-3*n) * b.^(-n);
dddt = we - 1;
dbdt = B * (1-b) - (we-1) * b;

dydt = [dddt; dbdt];
