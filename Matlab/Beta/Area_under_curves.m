clc
clear all
% close all

addpath(genpath('../../Data/ezyfit'))


% Area under a curve (no function involved)
% Example
% X = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85];
% rA = [-.0053, -.0052, -.0050, -.0045, -.0040, -.0033, -.0025, -.0018, -.00125, -.0010];
% plot(X,-1./rA)
% Int = trapz(X, -1./rA);

load Int_27June.mat
% B=load('Int_28August.mat');    % each 200 px
%  B=load('Integral_scale28Augu.mat');    % each 100 px 
B=load('Int_28AugustlatsMeas.mat');

figure
for n=1:6
    hold on
    plot(Int{1,n}(:,1),Int{1,n}(:,2))
%     plot(B.Int{1,n}(:,1),B.Int{1,n}(:,2))
end

for n=1:6
%     Integral(1,n) = trapz(Int{1,n}(:,1), Int{1,n}(:,2));
    Integral(1,n) = trapz(Int{1,n}(1:23,1), Int{1,n}(1:23,2));

end


for i=1:6
        Integral_28(1,i) = trapz(B.Int{1,i}(1:25,1), B.Int{1,i}(1:25,2));

end

z_integr_27June=[100 200 300 400 500 600];   % Array of the averaged window of 100px
% z_integr_28August=[100 200 400 600]; 
z_integr_28August=[100 200 300 400 500 600]; 

% Manually
% L_27=[64 80 96 112 128 144];

figure
hold on
plot(z_integr_27June,Integral,'b>')
% plot(z_integr_27June,L_27,'b>')
plot(z_integr_28August,Integral_28,'ro')
set(gca,'fontsize',14)
xlabel('z [px]')
ylabel('Lt [px]')


