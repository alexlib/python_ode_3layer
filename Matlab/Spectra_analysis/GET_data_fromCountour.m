clc
clear all
close all

% Open the figure
open U_ens_200-400.fig; %open your fig file, data is the name I gave to my file
D=get(gca,'Children'); %get the handle of the line object

open V_ens_200-400.fig; %open your fig file, data is the name I gave to my file
W=get(gca,'Children'); %get the handle of the line object


% Get Data from D
XData=get(D,'XData'); %get the x data
YData=get(D,'YData'); %get the y data
ZData=get(D,'ZData'); %get the y data
Data=[XData' YData' ZData']; %join the x and y data on one array nx2
%Data=[XData;YData]; %join the x and y data on one array 2xn

% Get data from W
WXData=get(W,'XData'); %get the x data
WYData=get(W,'YData'); %get the y data
WZData=get(W,'ZData'); %get the y data
WData=[WXData' WYData' WZData']; %join the x and y data on one array nx2


% Convert the data from pixels to m/s
dt=0.025;    % seconds  25000 micros
M=(2.5*10^-2)/350;           % from px to m

XData_conv=XData{1,1}.*M;
YData_conv=flipud(YData{1,1}.*M);
Vel_Data_conv=ZData{2,1}.*(M/dt);

WXData_conv=WXData{1,1}.*M;
WYData_conv=flipud(WYData{1,1}.*M);
WVel_Data_conv=WZData{2,1}.*(M/dt);

% check 
figure
hold on
contourf(XData_conv,YData_conv,Vel_Data_conv,50,'LineStyle',':')
% contourf(XData_conv,YData_conv,Vel_Data_conv,50)
xlabel('x [m]')
ylabel('z [m]')
h=colorbar;
title(h,'U [m/s]')
set(gca,'Ydir','reverse')
colormap(parula)
caxis([-0.015 0.015])
set(gca,'Fontsize',22)
title('Horizontal Velocity')

figure
hold on
contourf(WXData_conv,WYData_conv,WVel_Data_conv,50,'LineStyle',':')
% contourf(WXData_conv,WYData_conv,WVel_Data_conv,50,'LineStyle','none')
xlabel('x [m]')
ylabel('z [m]')
h=colorbar;
title(h,'V [m/s]')
set(gca,'Ydir','reverse')
colormap(parula)
caxis([-0.015 0.015])
set(gca,'Fontsize',22)
title('Vertical Velocity')


% % Overlap with the quiver 
% figure
% hold on
% contourf(XData_conv,YData_conv,Vel_Data_conv,100,'LineStyle','none')
% quiver(XData_conv,YData_conv,Vel_Data_conv,WVel_Data_conv,1.5)
% xlabel('x [m]')
% ylabel('z [m]')
% h=colorbar;
% title(h,'U [m/s]')
% set(gca,'Ydir','reverse')
% colormap(jet)
% 
% figure
% hold on
% contourf(WXData_conv,WYData_conv,WVel_Data_conv,100,'LineStyle','none')
% quiver(XData_conv,YData_conv,Vel_Data_conv,WVel_Data_conv,1.5)
% xlabel('x [m]')
% ylabel('z [m]')
% h=colorbar;
% title(h,'V [m/s]')
% set(gca,'Ydir','reverse')
% colormap(jet)

