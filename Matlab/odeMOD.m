function dfdt = odeMOD(t,f, w0, w2, b2, b10 , K, hgrid, beta, n, A)

% unpack vector
h=f(1); b1=f(2); he=f(3); be=f(4);


    
% here the simulation stops when h goes to zero, so it's nonsense

% be=(b1)*0.5;

db = abs(b2 - b1);
w_star=K./(h-hgrid).^(1);
Lt=beta*(h-hgrid);
Ri=(db.*Lt)./w_star.^2;
% Ri=(db.*he)./w_star.^2;

% I fit n and A from exp and Hopfinger
E=A*Ri.^(-n);
we=E.*w_star;


% h or he cannot be negative
if he <= 1e-3,  dhedt =0; else dhedt= w2 - we; end
if h  <= 1e-3,  dhdt  =0; else dhdt= we - w2; end % in dh/dt shall be also some 
% effect of the Q_0 and Q_3, right? 

% % Top layer 
% dhdt= we - w2;
db1dt= (1/h)*(-b1*dhdt + b10*w0 + be*we - b1*(w0+w2));

% Mid-layer
% dhedt= w2 - we;
dbedt =(1/he)* (-be*dhedt + b2*w2 - be*we);



% pack vector
dfdt = [dhdt; db1dt; dhedt; dbedt];

