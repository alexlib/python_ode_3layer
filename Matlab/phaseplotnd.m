function phaseplotnd(B, hg)

%% create phase portrait

N = 25;
y1 = linspace(0.01, 2, N); 
y2 = linspace(0.01, 2, N);
[Y1, Y2] = meshgrid(y1, y2);
U1 = zeros(size(Y1));
U2 = zeros(size(Y2));
for i=1:length(y1)
  for j=1:length(y2)
    U = box1nd(0, [Y1(j,i);Y2(j,i)], ...
               B, hg);
    U1(j, i) = U(1);
    U2(j, i) = U(2);
  end
end

% normalise vectors
U = sqrt(U1.^2 + U2.^2);
U1 = U1 ./ U;
U2 = U2 ./ U;

% draw "velocity field"
%figure(h)
quiver(Y1, Y2, U1, U2, 'k', 'AutoScaleFactor', 0.5);
hold all
plot(1, 1, 'o', 'linewidth', 2)
xlim([0,2])
ylim([0, 2])
xlabel('d')
ylabel('b')

% plot some trajectories
y0s = {[2, 1], [0.1, 0.6], [2, 0.2], [0.5, 2]};

for n = 1:length(y0s);
    y0 = y0s{n};
    [~, y] = ode45(@box1nd, [0 40], y0, [], B, hg);
    d = y(:, 1);
    b = y(:, 2);
    plot(d, b, 'linewidth', 2)
end