function [Ri_num,E_num,we_num,db_num]=entrainment_num(b1_num,b2,h,hgrid,beta,K,A,n,n_decay)

% the function has to run only for the numerical case. It is a bit
% different from the entrainment_law.m

db_num=abs(b2(1)- b1_num');

% urms parameterisation through the action grid
w_star=K./(h'-hgrid).^(n_decay);

% integral lenght scale as a measure of the thickness of the interface 
Lt=beta*(h'-hgrid);


% Build up the Richardson number 
Ri_num=(db_num.*Lt)./w_star.^2;

% Use n and A from Hopfinger
 E_num=A*Ri_num.^(-n);
 we_num=E_num.*w_star;
 
 