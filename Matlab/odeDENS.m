function dfdt = odeDENS(t,f, rho_ref,w0, w2, rho2, rho10, K, hgrid, beta, n, A)

% unpack vector
h=f(1); rho_1=f(2); he=f(3); rho_e=f(4);


% How to define the Richardson?
g=9.81;
drho = rho2 - rho_1;


w_star=K./(h-hgrid).^(1);
Lt=beta*(h-hgrid);
Ri=((g/rho_ref)*(drho.*Lt))./w_star.^2;

% I fit n and A from exp and Hopfinger
E=A*Ri.^(-n);
we=E.*w_star;



% h or he cannot be negative
if h  <= 0.001, dhdt = 0; else dhdt= we - w2; end

if he <= 0.001, dhedt=0; else dhedt= w2 - we; end


% % % Top layer 
% dhdt= we - w2;
% dhedt= w2 - we;

% Top layer
drho_1dt= (1/h)*(-rho_1*dhdt + rho10*w0 + rho_e*we - rho_1*(w0+w2));


% Mid-layer
drho_edt =(1/he)* (-rho_e*dhedt + rho2*w2 - rho_e*we);


% pack vector
dfdt = [dhdt; drho_1dt; dhedt; drho_edt];

