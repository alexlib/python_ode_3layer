% load ../Data/Exp_10May_2016.mat       % beta=0.15;      q1=76 ml/min   q2=6.5 ml/min
% Data.h=Data.h-0.02
% load ../Data/Exp_11Nov_2015.mat                      q1=87 ml/min   q2=5.6 ml/min

clc 
clear all
% close all

addpath(genpath('../Data/ezyfit'))

%% LOAD THE DATA FROM THE EXP

% For the first set of data we have measured the density via-pycnometer.
density_manually=false;

%  load ../Data/Exp_9March_2016.mat           % N5             q1=0 ml/min    q2=0 ml/min       (Not entirely sure about this test)
%  load ../Data/Exp_30March_2016_partA.mat     % N6a            q1=0 ml/min    q2=0 ml/min
%   load ../Data/Exp_30March_2016_partB.mat    % N6b           q1=87 ml/min   q2=6.5 ml/min  (It is detrainment. that why beta does not work)
%   load ../Data/Exp_28August_2016.mat        % N1          q1=40 ml/min   q2=40 ml/min
% load ../Data/Exp_2March_2016.mat       % N4      q1=76 ml/min   q2=6.5 ml/min


% In the following experiments we used the conductivity probe 
load ../Data/Exp_19June_2016.mat      % N2       q1=40 ml/min   q2=40 ml/min
%   load ../Data/Exp_27June_2016.mat      % N3       q1=40 ml/min   q2=40 ml/min



%% Set the Grid parameters
f=4;             % 1/s
M=0.025;         % m
St=0.02;         % m


%% Entrainment coefficients
n=1.5;
A=3.8;


% Calculate the density
[rho1]=conductivity_to_density(Data.Cond);

if  density_manually
    Data.rho1=Data.rho1_meas;
else   
 Data.rho1=1000*rho1;
 Data.rho1_meas=Data.rho1;
end

%%  Entrainment measurements
[Qout,Qout_toplayer,Qe_8,Qe_10,b1,b2,we,db]=mass_conservation(Data.rho1,Data.rho2,Data.time,Data.h,Data.Q1,Data.Q2,Data.rho10,Data.S,Data.g);

%% Grid Action Model  (K)
 C=0.181;  %(28 August)
% C=0.2546;  %(27 June)


[K]=grid_action(St,M,C,f);        % m2/s
% K=5.3200e-04;

%% Integral scale model (Lt=beta*z)
[Lt,beta]=integral_scale(Data.Q2,Data.h-Data.hgrid);       % Lt [m]

beta=0.11694;   %(28 August)


%% The entrainment law

% n_decay was measured by Turner/Hopfinger as 1-1.5
% This parameter should be measured experimentally as C and beta 
n_decay=1;

[Ri,E]=entrainment_law(db,Data.h,Data.hgrid,we,K,Lt,n_decay);


%% Build up the flow rate vectors only for the plot

q1_vec=ones(length(Data.time),1)*Data.Q1;
q2_vec=ones(length(Data.time),1)*Data.Q2;
Qout_sum=q1_vec+q2_vec;


%% 

figure
subplot(121)
hold on
plot(Data.time/3600,Data.h,'b-o')
xlabel('time [hr]')
ylabel('h [m]')

subplot(122)
hold on
plot(Data.time/3600,Data.rho1,'r->')
xlabel('time [hr]')
ylabel('\rho_{1} [g/mL]')


%%
figure
hold on
plot(Data.time,Data.h,'b-o')
xlabel('time [hr]')
ylabel('h [m]')
set(gca,'yscale','log','xscale','log')

%% Flow rates 

% Figure in ml/min
figure
hold on

plot(Data.time/3600,Qout_toplayer*(60/10^-6),'g-<','linewidth',1)
plot(Data.time/3600,q1_vec*(60/10^-6),'b-o','linewidth',1)
plot(Data.time/3600,q2_vec*(60/10^-6),'r->','linewidth',1)
plot(Data.time/3600,Qe_8*(60/10^-6),'m-o','linewidth',1)
plot(Data.time/3600,Qe_10*(60/10^-6),'k-+','linewidth',1)

xlabel('time [hr]')
ylabel('Flow rates in [mL/min]')
legend('Q_{out}','q1','q2','Qe_{8}','Qe_{10}')


%% Table
figure

subplot(211)
hold on
plot(Data.time/3600,Ri,'k-<','linewidth',1)
xlabel('time [hr]')
ylabel('Ri')

subplot(212)
hold on
plot(Data.time/3600,E,'r-+','linewidth',1)

xlabel('time [hr]')
ylabel('E= w_{e}/u^{*}')

%% For comparison between experiments

figure

subplot(221)
hold on
plot(Data.time/3600,Data.h,'b-o')
xlabel('time [hr]')
ylabel('h [m]')

subplot(222)
hold on
plot(Data.time/3600,Data.rho1,'r->')
xlabel('time [hr]')
ylabel('\rho_{1} [g/mL]')


subplot(223)
hold on
plot(Data.time/3600,Ri,'k-<','linewidth',1)
xlabel('time [hr]')
ylabel('Ri')

subplot(224)
hold on
plot(Data.time/3600,E,'r-+','linewidth',1)

xlabel('time [hr]')
ylabel('E= w_{e}/u^{*}')



%% E-Ri plot
figure
hold on
plot(Ri,E,'b-o','linewidth',1)

xlabel('Ri')
ylabel('E')


%% Comparison with literature (Hopfinger and Turner)

H=load('Hopfinger.txt');
RiH=H(:,2);
EH=H(:,3);

T=load('Turner_from_Hopfinger.txt');
RiT=T(:,2);
ET=T(:,3);


figure
hold on
plot(RiH,EH,'ro','Markersize',6,'MarkerFaceColor','r')
plot(RiT,ET,'k+')
plot(Ri,E,'c>','linewidth',1,'MarkerFaceColor','b')

xlabel('Ri')
ylabel('E')

set(gca,'XScale','log','YScale','log');
legend('Hopfinger','Turner','2pumps')


%% Double check
K_fit=K;
M_grid=0.025;
C_fit=K_fit./(St^(3/2)*M_grid^(1/2)*f)


%% Obuknov scale

% Lk=(K_fit^(3/4))./(Qe_8.^(1/4));
% 
% figure
% subplot(121)
% plot(Data.time/3600,Lk,'b-o','linewidth',2)
% xlabel('time [hr]')
% ylabel('L_{k}')
% 
% subplot(122)
% plot(Data.time/3600,Data.h./Lk,'r-+','linewidth',2)
% xlabel('time [hr]')
% ylabel('h./L_{k}')



% drho=Data.rho2-Data.rho1(end)

%% Calculate Buoyancy flux and plot in function of h(t)

% [b1_time,Flux_b1]=cal_buoyancyflux(Data.rho2,Data.rho1,q1_vec');
[b1_time,Flux_b1]=cal_buoyancyflux(Data.rho2,Data.rho1_meas,Qout_toplayer);

% entrainment density mass as the average ofthe two densities (the equations are written like this)
rho_e=(Data.rho2+Data.rho1)*0.5;
[be_time,Flux_be]=cal_buoyancyflux(Data.rho2,rho_e,Qe_8);

% Total flux 
% Total_flux=Flux_b1+Flux_be;     % B=wo*b

% St=stroke
St=0.02;f=4;

%%
Hop_Lin=load('Hopfinger_Linden1982.txt');

% close all

%%
figure
subplot(211)
hold on
plot(Hop_Lin(:,1),Hop_Lin(:,2),'g-','linewidth',2)
plot((St*f)^3./Flux_b1,Data.h*100,'b-o')
plot((St*f)^3./Flux_be,Data.h*100,'r->')
% plot((St*f)^3./Total_flux,Data.h*100,'k-^')
ylabel('h [cm]')
xlabel('(S*f)^3/B')
set(gca,'Xscale','log')
legend('Hopfinger','(S*f)^3/B1','(S*f)^3/Be')
set(gca,'fontsize',14)
box on
grid on

subplot(212)
hold on
plot(Flux_b1*10^4,Data.h*100,'b-o')
plot(Flux_be*10^4,Data.h*100,'r->')
% plot(Total_flux*10^4,Data.h*100,'k-^')
ylabel('h [cm]')
xlabel('B [cm2/s3]')
set(gca,'Xscale','log')
legend('B1','Be')
set(gca,'fontsize',14)
box on
grid on

%% Plot h versus (u0z0)^3/B0

u0_z0cub=(K_fit).^3;
Hop_Lin2plot=load('Hopfinger_Linden1982Plot2.txt');


figure
hold on
plot(Hop_Lin2plot(:,2),Hop_Lin2plot(:,3),'g-','linewidth',2)
plot(u0_z0cub./Flux_b1 *(10^8),Data.h*100,'b-o')
plot(u0_z0cub./Flux_be *(10^8),Data.h*100,'r->')
% plot(u0_z0cub./Total_flux *(10^8),Data.h*100,'k-^')
ylabel('h [cm]')
xlabel('(u0 z0)^3/B [cm4]')
set(gca,'Xscale','log')
set(gca,'Yscale','log')

legend('Hopfinger&Linden','(u0 z0)^3/B0','(u0 z0)^3/Be')
set(gca,'fontsize',14)
box on
grid on

%% Flux of Richardson number (see Hopfinger and Linden 1982)

[b0_time,Flux_b0]=cal_buoyancyflux(Data.rho2,Data.rho10,q1_vec');

z_int=linspace(0,Data.h(end),100);
B0=Flux_b0;
D=Data.h-Data.hgrid;

for n=1:length(Data.h)
Rich_flux(:,n)=(B0(1)*(D(n)-z_int)./D(n)).*(z_int.^2 ./(3*K_fit^3));
end

figure
for n=1:length(Data.h)
hold on
plot(Rich_flux(:,n),z_int./D(n))
    
end
set(gca,'ydir','reverse')
set(gca,'fontsize',14)
xlim([0 100])
xlabel('Ri_{fD}')
ylabel('z/D')

%% Fernando and Long 1988 
% D=alpha *(u^3/q0)

% They defined the buoyancy flux as q0=g* Q0 * (rho_out-rho_0) /(rho_ref A)
% The function down here is slightky different 
[b0_timeS,q0]=cal_buoyancyfluxFer(Data.rho2,Data.rho1,Data.rho10,q1_vec');

figure
plot(Data.time/3600,q0,'b-o')
set(gca,'fontsize',14)
xlabel('time')
ylabel('q0 [m2/s3]')

% 
Den=(K_fit^3./D.^3)./(q0);
Den2=(K_fit^3./D.^3)./(B0);

% close all
% 
figure
hold on
plot(Den,D,'bo')
% plot(Den2,D,'r-+')
set(gca,'fontsize',14)
xlabel('u^3/q0')
ylabel('D=h-hgrid')

Ind=D./Den;

figure
plot(Ind)
set(gca,'fontsize',14)
efmenu

%% Local Richardson Number From Fernando&Noh 1993

%  k_h=0.16e-6; % of water
k_h=0.61e-9;  % epsom salts m2/s

dh=0.1*(Data.h-Data.hgrid);
db_dz_0=(b1_time(1)-b2)./dh(1);

% Solution taken from Fernando
db_dzTime=db_dz_0*erf((Data.h-Data.hgrid)./((pi*k_h*Data.time).^0.5));

% Local Richardson number
Ri_loc=(db_dzTime.*Lt)./((K_fit./(Data.h-Data.hgrid)).^2);

figure
subplot(121)
plot(Data.time(2:end)/3600,db_dzTime(2:end),'b-+')
xlabel('time')
ylabel('db_dzTime')
set(gca,'fontsize',14)

subplot(122)
plot(Data.time(2:end)/3600,Ri_loc(2:end),'r-o')
xlabel('time')
ylabel('Ri Loc')
set(gca,'fontsize',14)


figure
plot(Ri_loc(2:end),E(2:end),'r>','linewidth',1,'MarkerFaceColor','r')
xlabel('Ri')
ylabel('E')
set(gca,'XScale','log','YScale','log');


%% Check Peclet and Richardson critical

Pe=(beta*K_fit)./(k_h)
Ric=Pe^(0.5)

nu=1.0034e-4;
Re_l=(K_fit*beta)/nu

%% diffusion dominant case

% dh=k_h/Data.Q2;   % [m]
% Ric2=Lt(end)./dh


% subplot(212)
% hold on
% plot(Flux_b1*10^4,Data.h*100,'b-o')
% plot(Flux_be*10^4,Data.h*100,'r->')
% plot(Total_flux*10^4,Data.h*100,'k-^')
% ylabel('h [cm]')
% xlabel('B [cm2/s3]')
% set(gca,'Xscale','log')
% legend('B0','Be','Btot')
% set(gca,'fontsize',14)
% box on
% grid on




%%

% C_matrix=load('Resume_exp_cvariable.txt');
% C_matrix(1,:)=[0.3 0 0 129 1129 1038 0.1786 405 0.00032];
% C_matrix(2,:)=[0.24 76 6.5  77.5 1073 1005 0.1614 300 0.00065];
% C_matrix(3,:)=[0.16 40 40  50 1050 1024 0.1342 96.42 0.0041];
% 

% % list of variables
% fprintf('C   Q1 [ml/min]     Q2 [ml/min]     Delta\rho_{ini}  \rho_{2} \rho_{1ss} h_{ss} Ri_{ss} E_{ss}\n')
% disp(C_matrix)

% figure
% subplot(231)
% hold on
% plot(C_matrix(:,2),C_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% plot(C_matrix(:,3),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('Q_{1} Q_{2} [ml/min]')
% ylabel('C')
% 
% subplot(232)
% hold on
% plot(C_matrix(:,4),C_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('\Delta\rho [kg/m3]')
% ylabel('C')
% 
% subplot(233)
% hold on
% plot(C_matrix(:,5),C_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('\rho_{2} [kg/m3]')
% ylabel('C')
% 
% subplot(234)
% hold on
% plot(C_matrix(:,7),C_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('h_{ss} [kg/m3]')
% ylabel('C')
% 
% subplot(235)
% hold on
% plot(C_matrix(:,8),C_matrix(:,1),'mo','MarkerFaceColor','m','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('Ri_{ss}')
% ylabel('C')
% 
% subplot(236)
% hold on
% plot(C_matrix(:,9),C_matrix(:,1),'mo','MarkerFaceColor','m','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('E_{ss}')
% ylabel('C')
% 
% %% 
% Beta_matrix=load('Resume_exp_beta.txt');
% 
% % C_matrix(1,:)=[0.3 0 0 129 1129 1038 0.1786 405 0.00032];
% % C_matrix(2,:)=[0.24 76 6.5  77.5 1073 1005 0.1614 300 0.00065];
% % C_matrix(3,:)=[0.16 40 40  50 1050 1024 0.1342 96.42 0.0041];
% % 
% 
% % % list of variables
% % fprintf('C   Q1 [ml/min]     Q2 [ml/min]     Delta\rho_{ini}  \rho_{2} \rho_{1ss} h_{ss} Ri_{ss} E_{ss}\n')
% % disp(C_matrix)
% 
% figure
% subplot(231)
% hold on
% plot(Beta_matrix(:,2),Beta_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% plot(Beta_matrix(:,3),Beta_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('Q_{1} Q_{2} [ml/min]')
% ylabel('\beta')
% 
% subplot(232)
% hold on
% plot(Beta_matrix(:,4),Beta_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('\Delta\rho [kg/m3]')
% ylabel('\beta')
% 
% subplot(233)
% hold on
% plot(Beta_matrix(:,5),Beta_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('\rho_{2} [kg/m3]')
% ylabel('\beta')
% 
% subplot(234)
% hold on
% plot(Beta_matrix(:,7),Beta_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('h_{ss} [kg/m3]')
% ylabel('\beta')
% 
% subplot(235)
% hold on
% plot(Beta_matrix(:,8),Beta_matrix(:,1),'mo','MarkerFaceColor','m','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('Ri_{ss}')
% ylabel('\beta')
% 
% subplot(236)
% hold on
% plot(Beta_matrix(:,9),Beta_matrix(:,1),'mo','MarkerFaceColor','m','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('E_{ss}')
% ylabel('\beta')

%% FIT the law 
% figure
% plot(Beta_matrix(:,3),Beta_matrix(:,1),'mo','MarkerFaceColor','m','MarkerEdgeColor','b')
% xlabel('Q2 [ml/min]')
% ylabel('\beta')




% figure
% hold on
% plot(C_matrix(:,2),C_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% plot(C_matrix(:,3),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('Q_{1} Q_{2} [ml/min]')
% ylabel('C')
% ax1 = gca; % current axes
% ax1.XColor = 'r';
% ax1_pos = ax1.Position; % position of first axes
% plot(C_matrix(:,7),C_matrix(:,1),'ko','MarkerFaceColor','k','MarkerEdgeColor','k')
% ax2 = axes('Position',ax1_pos,'XAxisLocation','bottom','Color','none');


% http://www.mathworks.com/matlabcentral/newsreader/view_thread/328686

% figure
% a=axes('units','normalized','position',[.1 .25 .8 .7],'xlim',[0 1200],'xtick',0:100:1200)
% hold on
% plot(C_matrix(:,2),C_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% plot(C_matrix(:,3),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% plot(C_matrix(:,4),C_matrix(:,1),'g>','MarkerFaceColor','g','MarkerEdgeColor','g')
% plot(C_matrix(:,5),C_matrix(:,1),'k+','MarkerFaceColor','k','MarkerEdgeColor','k')
% xlabel(a,'Q1,Q2 [ml/min]')
% b=axes('units','normalized','position',[.1 .15 .8 0.000001],'xlim',[0 140],'color','none')
% xlabel(b,'\Delta\rho [kg/m3]')
% c=axes('units','normalized','position',[.1 .1 .8 0.000001],'xlim',[0 1200],'color','none')
% xlabel(c,'\rho_{2} [kg/m3]')

% Find the maximum values and normalise them 
% C_2max=76; C_3max=40; C_4max=1129; C_5max=1073; C_7max=0.1786; C_8max=405;  C_9max=0.0041;  

% figure
% hold on
% plot(C_matrix(:,2)/C_2max,C_matrix(:,1),'bo','MarkerFaceColor','b','MarkerEdgeColor','b')
% plot(C_matrix(:,3)/C_3max,C_matrix(:,1),'gs','MarkerFaceColor','g','MarkerEdgeColor','g')
% plot(C_matrix(:,4)/C_4max,C_matrix(:,1),'r>','MarkerFaceColor','r','MarkerEdgeColor','r')
% plot(C_matrix(:,5)/C_5max,C_matrix(:,1),'m<','MarkerFaceColor','m','MarkerEdgeColor','m')
% plot(C_matrix(:,7)/C_7max,C_matrix(:,1),'c+','MarkerFaceColor','c','MarkerEdgeColor','c')
% plot(C_matrix(:,8)/C_8max,C_matrix(:,1),'k^','MarkerFaceColor','k','MarkerEdgeColor','k')
% plot(C_matrix(:,9)/C_9max,C_matrix(:,1),'yo','MarkerFaceColor','y','MarkerEdgeColor','y')

%% Fit the law
% figure
% plot(C_matrix(:,3),C_matrix(:,1),'mo','MarkerFaceColor','m','MarkerEdgeColor','b')
% xlabel('Q2 [ml/min]')
% ylabel('C')
% 
% figure
% plot(C_matrix(:,9),C_matrix(:,1),'mo','MarkerFaceColor','m','MarkerEdgeColor','b')
% xlabel('E_{ss}')
% ylabel('C')
% 
% figure
% plot(C_matrix(:,8),C_matrix(:,1),'mo','MarkerFaceColor','m','MarkerEdgeColor','b')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('Ri_{ss}')
% ylabel('C')
% 
% figure
% plot(C_matrix(:,6),C_matrix(:,1),'ko','MarkerFaceColor','k','MarkerEdgeColor','k')
% % plot(C_matrix(:,),C_matrix(:,1),'w>','MarkerFaceColor','w','MarkerEdgeColor','r')
% xlabel('\rho_{1ss} [kg/m3]')
% ylabel('C')

% figure
% subplot(7,2,1);grid on; box on;set(gca,'fontsize',14);ylim([0.09 0.15]);xlim([0 10]);ylabel('h');   
% subplot(7,2,2);grid on; box on;set(gca,'fontsize',14);ylim([980 1040]);xlim([0 10]);ylabel('\rho_{1}');
% subplot(7,2,3);grid on; box on;set(gca,'fontsize',14);ylim([0.09 0.15]);xlim([0 10]);ylabel('h');
% subplot(7,2,4);grid on; box on;set(gca,'fontsize',14);ylim([980 1040]);xlim([0 10]);ylabel('\rho_{1}');
% subplot(7,2,5);grid on; box on;set(gca,'fontsize',14);ylim([0.09 0.15]);xlim([0 10]);ylabel('h');
% subplot(7,2,6);grid on; box on;set(gca,'fontsize',14);ylim([980 1040]);xlim([0 10]);ylabel('\rho_{1}');
% subplot(7,2,7);grid on; box on;set(gca,'fontsize',14);ylim([0.09 0.15]);xlim([0 10]);ylabel('h');
% subplot(7,2,8);grid on; box on;set(gca,'fontsize',14);ylim([980 1040]);xlim([0 10]);ylabel('\rho_{1}');
% subplot(7,2,9);grid on; box on;set(gca,'fontsize',14);ylim([0.09 0.15]);xlim([0 10]);ylabel('h');
% subplot(7,2,10);grid on; box on;set(gca,'fontsize',14);ylim([980 1040]);xlim([0 10]);ylabel('\rho_{1}');
% subplot(7,2,11);grid on; box on;set(gca,'fontsize',14);ylim([0.09 0.15]);xlim([0 10]);ylabel('h');
% subplot(7,2,12);grid on; box on;set(gca,'fontsize',14);ylim([980 1040]);xlim([0 10]);ylabel('\rho_{1} ');
% subplot(7,2,13);grid on; box on;set(gca,'fontsize',14);ylim([0.09 0.15]);xlim([0 10]);ylabel('h');xlabel('time')
% subplot(7,2,14);grid on; box on;set(gca,'fontsize',14);ylim([980 1040]);xlim([0 10]);ylabel('\rho_{1}');xlabel('time')
% 

% figure
% subplot(121);grid on; box on;set(gca,'fontsize',14);ylim([0.09 0.15]);xlim([0 10]);ylabel('h [m]');     xlabel('time [hr]') 
% subplot(122);grid on; box on;set(gca,'fontsize',14);ylim([980 1040]);xlim([0 10]);ylabel('\rho_{1} [kg/m^{3}]');xlabel('time [hr]')


