function [Ri,E]=entrainment_law(db,h,hgrid,we,K,Lt,n_decay)


% From the K-action fitting or modeling, we calculate umrs (here I called it w_star)
w_star=K./(h-hgrid).^(n_decay);


% Fit with exponential curve
% a=4.175e-05;
% b=0.10673;
% nexp=-2.6;
% w_star=a*(h-hgrid+b).^(nexp);

% By using the Turner/Hopfinger entrainment law
Ri=(db.*Lt)./w_star.^2;
E=abs(we)./w_star;

