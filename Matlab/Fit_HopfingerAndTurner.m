clc 
clear all
close all

H=load('Hopfinger.txt');
RiH=H(:,2);
EH=H(:,3);

T=load('Turner_from_Hopfinger.txt');
RiT=T(:,2);
ET=T(:,3);


figure
hold on
plot(RiH,EH,'ro','Markersize',6,'MarkerFaceColor','r')
plot(RiT,ET,'k+')
% plot(Ri,E,'c>','linewidth',1,'MarkerFaceColor','b')

xlabel('Ri')
ylabel('E')

set(gca,'XScale','log','YScale','log');
legend('Hopfinger','Turner','2pumps')

