function [Ri,E,we,drho]=entrainment_numDENS(rho0,rho1_num,rho2,h,hgrid,beta,K,A,n,n_decay)

drho=(rho2- rho1_num);

% urms parameterisation through the action grid
w_star=K./(h'-hgrid).^(n_decay);

% integral lenght scale as a measure of the thickness of the interface 
Lt=beta*(h'-hgrid);

g=9.81;

w_star=K./(h-hgrid).^(1);
Lt=beta*(h-hgrid);

Ri=((g/rho0)*(drho.*Lt))./w_star.^2;
% Build up the Richardson number 

% Use n and A from Hopfinger
 E=A*Ri.^(-n);
 we=E.*w_star;
 
 