function [b_timeS,Flux_b]=cal_buoyancyfluxFer(rho_ref,rho_time,rho_input,Flux_vol)

% This function calculate the stabilizing buoyancy flux according to the
% definition of Fernando and Long 1988


% calculate buoyancy from the density
S=0.02;
g=9.81;
w0=Flux_vol./S;

Flux_b=g*w0.*(rho_time-rho_input)./(rho_ref);

b_timeS=g*(rho_time-rho_input)./(rho_ref);