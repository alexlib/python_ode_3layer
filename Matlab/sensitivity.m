function [dRi_total, dE_total]=sensitivity(Delta_h,Delta_hgrid,Delta_w2,Delta_beta,Delta_C,Delta_db,h,db,C,beta,S,M,f,hgrid,w2)

% This function calculate the sensitivity factors of Ri and E for fixed
% frequency and Stroke

dRi_db=(beta*(h-hgrid).^2)./(C*S^(3/2)*M^(1/2)*f)^2;
dRi_dbeta=db*(h-hgrid).^2 /(C*S^(3/2)*M^(1/2)*f)^2;
dRi_dh=2*beta*db*(h-hgrid)/(C*S^(3/2)*M^(1/2)*f)^2;
dRi_dhgrid=-2*beta*db*(h-hgrid)/(C*S^(3/2)*M^(1/2)*f)^2;
dRi_dC=-2*beta*db*(h-hgrid)^2 / (C^3)*(S^(3/2)*M^(1/2)*f)^2;

dE_dh=w2 / (C*S^(3/2)*M^(1/2)*f);
dE_dhgrid=-w2 / (C*S^(3/2)*M^(1/2)*f);
dE_dC=w2*(h-hgrid) / (C^2)*(S^(3/2)*M^(1/2)*f);
dE_dw2=(h-hgrid) / (C*S^(3/2)*M^(1/2)*f);

dRi_total=sqrt((dRi_db*Delta_db)^2 + (dRi_dbeta*Delta_beta)^2 +  (dRi_dh*Delta_h)^2 +(dRi_dhgrid*Delta_hgrid)^2 +(dRi_dC*Delta_C)^2 );
dE_total=sqrt((dE_dh*Delta_h)^2 +(dE_dhgrid*Delta_hgrid)^2 + (dE_dC*Delta_C)^2 + (dE_dw2*Delta_w2)^2);

