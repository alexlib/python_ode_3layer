% clear all
% close all

hg = 0.065;

%% create phase portrait

Bs = [0.1 1 2 10];
% Bs = [0.1 5 10 20];

figure
for n=1:length(Bs)
    B = Bs(n);
    subplot(2,2,n);
    phaseplotnd(B, hg);
    title(['B = ', num2str(B, '%8.1f')])
end

%% Plot our single case
% Bexp=14.40;
Bexp=3.17
hg = 0.065;

figure
    phaseplotnd(Bexp, hg);
    title(['B = ', num2str(Bexp, '%8.1f')])


%% function phaseplotnd

type phaseplotnd

%% function box1nd

type box1nd

