function [K]=grid_action(St,M,C,f)

% The following function fit the K action parameter based on the Long and Hopfinger 
% formulations
% urms=K/z  where K=C St^3/2 M^(1/2) f

 K=C*St^(3/2) * M^(1/2)* f;

% nu=1.5215e-6;  % m2/s
% K=C*((f^2)/(nu)) * M^2 * St^2;
