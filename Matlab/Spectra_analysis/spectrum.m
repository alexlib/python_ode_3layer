function [xfft,magn,Energy_spectra,freq,xdft]=spectrum(vel_data)

% Calculate the Fft of the t.velocity and do not pass from the autocorrelations
% To compute the spectrum. E=2*abs(u^)

Fs=200;      % sampling frequency 
Ts=1/Fs;      % sampling period 

Final_t=400; % total time in seconds 

dt=0:Ts:Final_t-Ts;     % duration of the signal
nfft=length(vel_data);   % lenght of the time domain signal 
nfft2=2^nextpow2(nfft);   % lenght of the signal power of 2
ff=fft(vel_data,nfft2);

%take the magnitude of half spectrum
fff=ff(1:nfft2/2);

% normalised amplitude?
% fff=fff/max(fff);
magn=abs(fff);

% frequency
xfft=Fs*(0:nfft2/2-1)/nfft2;

Energy_spectra=2*(magn.^2);

 

% Fs = 200;
% t = 0:1/Fs:1-(1/Fs);
% x = vel_data;
% xdft = (1/length(x))*fft(x);
% freq = -1000:(Fs/length(x)):1000-(Fs/length(x));
% 

% figure
% plot(xfft,magn)
% xlabel('Frequency [Hz]')
% ylabel('Normalized Amplitude')