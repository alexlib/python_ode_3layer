function [Qout,Qout_toplayer,Qe_8,Qe_10,b1,b2,we,db]=mass_conservation(rho1,rho2,time,h,Q1,Q2,rho10,S,g)



%% Define the buoyancy. Choose b2=0
b2=g*(rho2 - rho2)/rho2;
b1=g*(rho2 - rho1)/rho2;

db=abs(b1-b2);

%% Gradient does the same faster and with 2nd order central difference
dhdt = gradient(h,time);
drho1dt = gradient(rho1,time);
db1dt = gradient(abs(b1),time);

%%  Mass conservation

% Total mass conservation
Qout = (rho10 * Q1 + rho2 * Q2)./rho1;

% Mass conservation top layer. The only way to estimate it, It is
% approximating rhoe* Qe = rho2 * Q2 from the beginning.
Qout_toplayer=(rho10 * Q1 + rho2 * Q2  - dhdt.*rho1*S - h.*drho1dt*S )./rho1;


% Good. Top layer 
Qe_8 = 2*(dhdt.*rho1*S + h.*drho1dt*S - rho10*Q1 + rho1.*Qout)./(rho1 + rho2); % corrected


% Good too. Bottom layer 
Qe_10 = 2*(rho2*dhdt*S + Q2*rho2)./((rho1 + rho2));   %eq. 13 


we=Qe_8./S;
