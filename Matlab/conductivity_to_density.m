function [Rho_meas]=conductivity_to_density(Cond_meas);

% This function is based on the conductivity probe calibration
% It transforms the measured conductivity ito density

Rho_meas=0.0013*Cond_meas + 0.9951;

