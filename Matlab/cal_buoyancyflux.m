function [b_time,Flux_b]=cal_buoyancyflux(rho_ref,rho_time,Flux_vol)

% calculate buoyancy from the density

g=9.81;
b_time=g*(rho_ref-rho_time)./(rho_ref);

S=0.02;
w0=Flux_vol./S;

Flux_b=b_time.*w0;
