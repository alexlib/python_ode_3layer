clc
clear all
close all
set(0,'defaulttextinterpreter','latex')

%% 
warning off

% Choose the density of the boottom layer
RhEpsom_chose=1.025;  % g/ml

disp('PLEASE USE g/ml TO CALCULATE THE CONCETRATION OF THE SOLUTIONS')

%% Build up the concentration vectors
n_point=100;
Ca=linspace(0,1,n_point);
Cb=linspace(0,1,n_point);

% Epsom Salt density / refractive index
Rho_Epsom = 0.99+ 0.49762*Ca;             % mine!! right

% n_epsom = 0.1133*Ca + 1.3323;           % my doubt
Ca_chosen=(RhEpsom_chose-0.99)/0.49762;


% from Krug  (No difference here) 
% Rho_Epsom = 0.8*Ca + 0.998;               % Krug == fit my measurements 
n_epsom= 0.175*Ca + 1.333;                % Krug == fit my measurements. This is the big difference!!!
% n_ethanol=0.0563*Cb + 1.333;                % coincident also with the web

n_ethanol=0.0563*Cb + 1.333;                % measured




% Daviero 
% Rho_Epsom = (998 + 0.681*(Ca*100))/1000;             % mine!! right
% Ca_chosen=(RhEpsom_chose-0.99)/0.681;
% n_epsom=1.3329 + 0.000171*(Ca*100);
% Rho_ethanol=(998-0.161*Cb*100)/1000;
% n_ethanol=1.3329 + 0.00006*(Cb*100);

% Ethanol density /refractive index
% Rho_ethanol = -0.216*Cb + 0.99;             % Krug 


% Measured on 22 August 2016
Rho_ethanol= -0.1203*Cb + 0.99;


figure
subplot(121)
hold on
plot(Ca,Rho_Epsom,'g')
plot(Cb,Rho_ethanol,'b')
ylabel('\rho_{a},\rho_{b}  [g/mml]')
xlabel('C_a , C_b')
legend('C_a=Epsom salts','C_b=Ethanol','$\rho_{a}=0.99 + 0.49762*Ca$','$\rho_{b}=-0.216*Cb + 1.0095$')

subplot(122)
hold on
plot(Ca,n_epsom,'g')
plot(Cb,n_ethanol,'b')
ylabel('\n_{a},n_{b}')
xlabel('C_a , C_b')
ylim([1.33 1.38])
legend('C_a=Epsom salts','C_b=Ethanol')


% Define the ratio vectors
Ratio_Ca_Cb=Ca./Cb;
Ratio_rhoa_rhob=Rho_Epsom./Rho_ethanol;

%% Generate a table where Cb is changing continously and Ca is fixed (As in the excel file)
% leg{:}={'C_a=0.26(sat)','C_a=0.2','C_a=0.16','C_a=0.12','C_a=0.08','C_a=0.04'}

% From zero to the saturation C_a=0.26(sat)
n_columns=n_point;
Ca_fixed=linspace(0.02,0.26,n_point); 

Rhoa_fixed=0.99+ 0.49762*Ca_fixed;
% na_fixed=0.1133*Ca_fixed + 1.3323;

% na from Krug
% Rhoa_fixed=0.99+ 0.8*Ca_fixed;
na_fixed=0.175*Ca_fixed + 1.333;



for n=1:length(Ca_fixed)
    
    Ratio_Ca_Cb_ite(:,n)=Ca_fixed(n)./Cb;
    Ratio_rhoa_rhobIte(:,n)=Rhoa_fixed(n)./Rho_ethanol;
    Ratio_na_nb(:,n)=na_fixed(n)./n_ethanol;
    Ratio_rhob_rhoAInv(:,n)=Rho_ethanol./Rhoa_fixed(n);

end


%%


figure
col='bgrmcy';

leg{:}={'C_a=0.04','C_a=0.08','C_a=0.12','C_a=0.16','C_a=0.2','C_a=0.26(sat)'};

for n=1:length(Ca_fixed)
    
    % Mark the dots with the same index of refraction
    Ind(n)=find(Ratio_na_nb(:,n) < 1,1,'first');

    subplot(1,2,1)
    hold on
    plot(Ratio_Ca_Cb_ite(:,n),Ratio_rhob_rhoAInv(:,n))
    plot(Ratio_Ca_Cb_ite(Ind(n),n),Ratio_rhob_rhoAInv(Ind(n),n),'ro')
    xlabel('C_a/C_b')
    ylabel('\rho_{B}/\rho_{A}')
    xlim([0 1])
    set(gca,'fontsize',14)

    
    subplot(1,2,2)
    hold on
    plot(Ratio_Ca_Cb_ite(:,n),Ratio_na_nb(:,n))
    xlabel('C_a/C_b')
    ylabel('n_{A}/n_{B}')
    xlim([0 1])
    set(gca,'fontsize',14)
   
end
%     legend(leg{1,1}{1,:})

for n=1:length(Ca_fixed)
   subplot(1,2,2)
    plot(Ratio_Ca_Cb_ite(Ind(n),n),1,'ro')
end


% We identify on what line we are moving.
Ind_concA=find(Ca_fixed > Ca_chosen,1,'first');
Index_match=Ind(Ind_concA);
Identified_curve=Ratio_Ca_Cb_ite(:,Ind_concA);
Identified_Ratio_rhobrhoAInv=Ratio_rhob_rhoAInv(:,Ind_concA);

Match_pointDens=Ratio_rhob_rhoAInv(Index_match,Ind_concA);
Match_pointCon=Ratio_Ca_Cb_ite(Index_match,Ind_concA);

leg_1=['Ca=',num2str(Ca_chosen)];
figure
hold on
plot(Identified_curve,Identified_Ratio_rhobrhoAInv)
xlabel('C_a/C_b')
ylabel('\rho_{B}/\rho_{A}')
xlim([0 1])
set(gca,'fontsize',14)
legend(leg_1)
plot(Match_pointCon,Match_pointDens,'ro')

% Calculate the quantities of solution B
Cb_matched=Ca_chosen./Match_pointCon;
% Rho_ethanol_matched = -0.216*Cb_matched + 0.99;
% n_ethanol_matched=-0.0554*Cb_matched.^2 + 0.0831*Cb_matched + 1.3318;
% n_epsom_matched = 0.1133*Ca_chosen + 1.3323;

% Krug
% Rho_ethanol_matched = -0.216*Cb_matched + 0.99;


Rho_ethanol_matched= -0.1203*Cb_matched + 0.99;
n_epsom_matched = 0.175*Ca_chosen + 1.333;

n_ethanol_matched=0.0563*Cb_matched + 1.333;



density_ratioMatch=RhEpsom_chose./Rho_ethanol_matched;
dn=abs(n_epsom_matched-n_ethanol_matched);
drho=RhEpsom_chose-Rho_ethanol_matched;

%% =========================================================================
% Compute the amount of Volume in the two buckets
% =========================================================================
dens_w=1000; %kg/m^3
% 
%Va_m=input('Va  [m^3] =');
%Vb_m=input('Vb  [m^3] =');

% % Just for the test
Va_m=0.0002;
Vb_m=0.0001;

mmw_a=Va_m*dens_w; %[kg]
m_A=-(mmw_a*Ca_chosen/(Ca_chosen-1));  % kg

mmw_b=Vb_m*dens_w; %[kg]
m_B=-(mmw_b*Cb_matched/(Cb_matched-1));

% conversion
% Va_c=Va_m*10^6;   %[ml]
% Vb_c=Vb_m*10^6;   %[ml]

Va_liters=Va_m*1000;
Vb_liters=Vb_m*1000;


% =========================================================================
% PRINT TABLE OF THE PARAMETERS
% =========================================================================
fprintf('%12s %12s %12s %12s %12s %12s %12s %12s %12s\n', ...
'C_a', 'C_b', 'C_a/C_b', '\rho_a [g/ml]', '\rho_b [g/ml]','drho','\rho_a/\rho_b','n_a','n_b','dn')
fprintf('%12.2f %12.2e %12.2e %12.2e %12.2f %12.2e %12.2e %12.2e %12.2e\n', ...
Ca_chosen, Cb_matched, Match_pointCon,RhEpsom_chose,Rho_ethanol_matched,drho,density_ratioMatch,n_epsom_matched,n_ethanol_matched,dn)
set(gca,'fontsize',14)

% fprintf('%12s %12s %12s %12s\n', ...
% 'Va_w [ml]', 'Vb_w [ml]', 'ma [g]', 'mb [ml]')
% fprintf('%12.2f %12.2f %12.2e %12.2e\n', ...
%     Va_c, Vb_c,m_A,m_B)
% set(gca,'fontsize',14)

figure
uitable('Data', [Ca_chosen,Cb_matched,Match_pointCon,RhEpsom_chose,Rho_ethanol_matched,drho,density_ratioMatch,n_epsom_matched,n_ethanol_matched,dn], 'ColumnName', {'C_a', 'C_b', 'C_a/C_b', '\rho_a [g/ml]', '\rho_b [g/ml]','drho','\rho_a/\rho_b','n_a','n_b','dn'}, 'Position', [20 100 800 50],'FontSize',13);
uitable('Data', [Va_liters, Vb_liters,m_A,m_B], 'ColumnName', {'Va_w [L]', 'Vb_w [L]', 'ma [kg]', 'mb [L]'}, 'Position', [20 200 400 50],'FontSize',13);

figure
uitable('Data', [Ca_chosen,Cb_matched,Match_pointCon,RhEpsom_chose,Rho_ethanol_matched,drho,density_ratioMatch,n_epsom_matched,n_ethanol_matched,dn], 'ColumnName', {'C_a', 'C_b', 'C_a/C_b', '\rho_a [g/ml]', '\rho_b [g/ml]','drho' ,'\rho_a/\rho_b','n_a','n_b','dn'}, 'Position', [20 100 850 50],'FontSize',13);
uitable('Data', [Va_liters*1000, Vb_liters*1000,m_A*1000,m_B*1000], 'ColumnName', {'Va_w [ml]', 'Vb_w [ml]', 'ma [g]', 'mb [mL]'}, 'Position', [20 200 400 50],'FontSize',13);


%% Plot the index matched map

% % [n_Eth,n_salt]=plot_index_matchMap(Ca_chosen, Cb_matched,Va_liters,Vb_liters);
% 
% [C,ia,ib] = intersect(n_epsom,n_ethanol);
% 
% dz=0.2;
% g=9.81;
% 
% % Create an array of the mass for the chosen volume
% mass_eps=(Rho_Epsom*1000)*Va_c;  
% mass_eth=(Rho_ethanol*1000)*Vb_c;  
% 
% N2=zeros(length(mass_eps),length(mass_eth));
% N=N2;
% 
% for i=1:length(mass_eps)
%     for j=1:length(mass_eth)
% %         
% drho(i,j)=(Rho_Epsom(i)-Rho_ethanol(j));
% rho_0(i,j)=(Rho_Epsom(i) + Rho_ethanol(j))*0.5*1000;        
% 
% 
% % n_salt(i)=1.3330 + 0.20286.*ca(i);
% % n_Eth(j)=1.3330 + 0.14445.*cb(j);
% 
% %  n_salt(i)=1.3329 + 0.000171.*ca(i);
% %  n_Eth(j)=1.3329 + 0.00006.*cb(j);
% 
% n_epsom(i)= 0.1133*Ca(i) + 1.3323;   
% n_ethanol(j)=-0.0554*Cb(i).^2 + 0.0831*Cb(i) + 1.3318;  
%  
% 
% dn(i,j)=abs(n_epsom(i)-n_ethanol(j));
% 
% N2(i,j)=((g./rho_0(i,j)).*(drho(i,j)./dz));
% N(i,j)=(N2(i,j)).^(0.5);
% 
%     end
% end
% 
% 
% [N_min,INdx]=min(dn);
% INdz=[1:1:length(Rho_Epsom)];
% 
% 
% % White line N=1 [1/s]
% White_line=zeros(length(mass_eps),length(mass_eth));
% 
% % RED LINE dn=0
% Red_line=zeros(length(mass_eps),length(mass_eth));
% 
% for n=1:length(Rho_Epsom)
% White_line(INdx(n),INdz(n))=1;
% end
% 
% [IndXX,IndZZ]=find(White_line>0);

% figure
% hold on
% % contourf(ma,mb,real(N),200,'LineStyle','none')
% contourf(ma,mb,dn,200,'LineStyle','none')
% plot(ma(IndZZ),mb(IndXX),'w-','linewidth',2)
% h=colorbar;
% xlabel('ma [kg]')
% ylabel('mb [kg] (Not liters!!)')
% ylabel(h, '\Delta n')
% % ylabel(h, 'N 1/s')
% title('Solutes weights in 10 Liters of fresh water')

% figure
% hold on
% % plot(mass_eps,mass_eth)%,'wo','linewidth',2)
% contourf(mass_eps,mass_eth,dn,200,'LineStyle','none')
% % plot(mass_eps(IndZZ),mass_eth(IndXX),'w-','linewidth',2)
% h=colorbar;
% ylabel('Liters of Ethanol [L]')
% xlabel('ma [kg]')
% ylabel(h, '\Delta n')
% ylabel(h, 'N 1/s')
% title('Solutes weights in ... Liters of fresh water')
% ylim([0 3])






%% Plot the diffusion of the density profiles in time
% 
% z=[-4.5/2:0.1:4.5/2];     % in cm 
% t=[0:3600:86400];              % in seconds, come cambia il profilo ogni 60 minuti in un giorno
% 
% tl=length(t);
% zl=length(z);
% 
% ar=1.0054;
% br=0.38407;
% an=0.20286;
% bn=0.14445;
% ka=0.61e-5;   % cm^2/s
% kb=0.45e-5;   % cm^2/s 
% 
% for i=1:tl
%     for j=1:zl
% Rho_pad(j,i)=((br/bn)*erf(z(j)/(2*(kb*t(i))^(0.5)))-(ar/an)*erf(z(j)/(2*(ka*t(i))^(0.5))))*(((ar/an)-(br/bn))^(-1));
% n_ad(j,i)=((0.5)*erf(z(j)/(2*(kb*t(i))^(0.5)))-(0.5)*erf(z(j)/(2*(ka*t(i))^(0.5))));
%     end
% end
% 
% skip=4;
% 
% 
% col_2='bgrmcyk';
% vect=[0,4,8,12,16,20,24];
% 
% figure (5)
% 
% for n=1:length(vect)
%     
%     leg{n}=[num2str(vect(n)) ' - hours']
%       
%     subplot(1,2,1)
%     hold on
%     plot(n_ad(:,(n*skip)-3),z,col_2(n))
%     xlabel('(n-n_{A})/(n_{A}-n_{w})')
%     ylabel('z [cm]')
%     legend(leg)
%     
%     subplot(1,2,2)
%     hold on
%     plot(Rho_pad(:,(n*skip)-3),z,col_2(n))
%     xlabel('(\rho-\rho_{m})/(\rho_{A}-\rho_{m})')
%     ylabel('z [cm]')
%     legend(leg)
% end
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
