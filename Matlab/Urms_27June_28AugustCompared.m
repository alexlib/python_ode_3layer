% Urms 27 June and 28 August compared 

clc
clear all
close all

% Add ezyfit
addpath(genpath('../Data/ezyfit'));

Urms_27June=load('../Data/2layers_27June16/Exp_27June/PIV_27June_ENTIRE/RMS_27June.mat'); 
Urms_28August=load('../Data/2layers_28August16/RMS_28August16.mat');

% Conversion 
dt=0.025;    % seconds  25000 micros
M=(2.5*10^-2)/350;           % from px to m


%% Test 27 June 2016
dy_27June=510*M; 


for n=1:4
z{n}=fliplr(Urms_27June.RMS{1,n}.y*M) - dy_27June;
end


% Run the K action script and calculate the virtual origin for the correct
% fitting 
K_fit=4.95e-04;    % m2/s
zvu=-2.86e-02;      % m
u_approx=K_fit./(z{n}-zvu);  

figure
hold on
for n=2:4
hold on
plot(z{n},(Urms_27June.RMS{1,n}.u*(M/dt)))

end

hold on
% plot(z{1},Grid_action,'k','linestyle','--','linewidth',2)
plot(z{1},u_approx,'b','linestyle','--','linewidth',2)

xlabel('z [m]')
ylabel('urms [m/s]')
set(gca,'Fontsize',16)
box on
legend('2.15 hr','3 hr','4 hr','u_{approx}=K_{fit}/(z-z_{vu})')
title('urms - 27 June 2016')

%% Test 28 August 2016

dy_28Aust=80*M; 


for n=1:3
z_2{n}=flipud(Urms_28August.RMS{1,n}.y*M) + dy_28Aust;
end


% Run the K action script and calculate the virtual origin for the correct
% fitting 
K_fit=3.24e-04;    % m2/s
zvu=-6.35e-03;      % m
u_approx_2=K_fit./(z_2{n}-zvu);  

col={'b','g','r','k'};

figure
hold on
for n=1:3
hold on
plot(z_2{n},(Urms_28August.RMS{1,n}.u*(M/dt)),col{n})

end

hold on
% plot(z{1},Grid_action,'k','linestyle','--','linewidth',2)
plot(z_2{1},u_approx_2,'r','linestyle','--','linewidth',2)

xlabel('z [m]')
ylabel('urms [m/s]')
set(gca,'Fontsize',16)
box on
% legend('2.15 hr','3 hr','4 hr','u_{approx}=K_{fit}/(z-z_{vu})')
% title('urms - 28 August 2016')

C=0.1810;
St=0.02;
Mgrid=0.025;
f=4;
K_theory=C*St.^(3/2)*Mgrid^(1/2)*f;


%% 27 June 
figure
subplot(121)
hold on
for n=2:4
hold on
plot(z{n},(Urms_27June.RMS{1,n}.u*(M/dt)),col{n})

end

hold on
% plot(z{1},Grid_action,'k','linestyle','--','linewidth',2)
plot(z{1},u_approx,'b','linestyle','--','linewidth',2)

xlabel('z [m]')
ylabel('urms [m/s]')
set(gca,'Fontsize',16)
box on

subplot(122)
hold on
for n=2:4
hold on
plot(z{1,n},(Urms_27June.RMS{1,n}.v*(M/dt)),col{n})

end

hold on
% plot(z{1},Grid_action,'k','linestyle','--','linewidth',2)
% plot(z_2{1},u_approx_2,'r','linestyle','--','linewidth',2)

xlabel('z [m]')
ylabel('vrms [m/s]')
set(gca,'Fontsize',16)
box on


%% 28 August

% figure
% subplot(121)
% hold on
% for n=1:3
% hold on
% plot(z_2{n},(Urms_28August.RMS{1,n}.u*(M/dt)),col{n})
% 
% end 
% xlabel('z [m]')
% ylabel('urms [m/s]')
% set(gca,'Fontsize',16)
% box on
% 
% subplot(122)
% hold on
% for n=1:3
% hold on
% plot(z_2{n},(Urms_28August.RMS{1,n}.v*(M/dt)),col{n})
% 
% end
% 
% hold on
% % plot(z{1},Grid_action,'k','linestyle','--','linewidth',2)
% % plot(z_2{1},u_approx_2,'r','linestyle','--','linewidth',2)
% 
% xlabel('z [m]')
% ylabel('vrms [m/s]')
% set(gca,'Fontsize',16)
% box on

%% PLOT the enrgy flux in both cases 
% Consider that Long said that q2 w1 is proportional to urms^3

% test 28 August 2016

figure
hold on
for n=1:3
    
q2w_o{1,n}=u_approx_2.^3;
q2_ss{1,n}=((Urms_28August.RMS{1,n}.u*(M/dt)).^2 + (Urms_28August.RMS{1,n}.v*(M/dt)).^2).*0.5;
flux_28{1,n}=q2_ss{1,n}.*Urms_28August.RMS{1,n}.v*(M/dt);

% normalised quantities
u28ugust_nomr{1,n}=(Urms_28August.RMS{1,n}.u*(M/dt))./(Urms_28August.RMS{1,n}.v*(M/dt));


hold on
plot(flux_28{1,n}./q2w_o{1,n},z_2{n},col{n},'linestyle','--')

end 
% xlabel('z [m]')
% ylabel('[q''^2w'']_{ss}/[q''^2w'']_o')
% set(gca,'Fontsize',16)
% box on

hold on
for n=2:4
    
q2w_o27{1,n}=u_approx.^3;
q2_ss27{1,n}=((Urms_27June.RMS{1,n}.u*(M/dt)).^2 + (Urms_27June.RMS{1,n}.v*(M/dt)).^2).*0.5;
flux_27{1,n}=q2_ss27{1,n}.*Urms_27June.RMS{1,n}.v*(M/dt);

% normalised quantities
u27June_nomr{1,n}=(Urms_27June.RMS{1,n}.u*(M/dt))./(Urms_27June.RMS{1,n}.v*(M/dt));


hold on
plot(flux_27{1,n}./q2w_o27{1,n},z{n},col{n})

end 
ylabel('z [m]')
xlabel('[q''^2w'']_{ss}/[q''^2w'']_o')
set(gca,'Fontsize',16)
set(gca,'ydir','reverse')
box on

figure
hold on
for n=1:3

plot(q2_ss{1,n},z_2{n},col{n},'linestyle','--')

end
for n=2:4

plot(q2_ss27{1,n},z{n},col{n})

end

ylabel('z [m]')
xlabel('q''^2 [m^2/s^2]')
set(gca,'Fontsize',16)
set(gca,'ydir','reverse')
box on

%% Create a shifted plot in reference to the interface position
% 28 August
z_int_h28=[0.07307 0.07536 0.0787];  % best
z_int_v28=z_int_h28; 
h=[0.115000000000000,0.135000000000000,0.140000000000000,0.145000000000000,0.145000000000000,0.145000000000000];
hgrid=0.065;
delta28=h-hgrid;


beta=0.1165;
L_h28=beta*delta28;
L_v28=0.155*delta28;  % This is valid in homogenous turbulence??? Kit and Fernando
L_v28=L_h28;

for n=1:3
z_star28{n}=(z_int_h28(n)-z_2{n})./L_h28(n);
z_starV28{n}=(z_int_v28(n)-z_2{n})./L_v28(n);
end

% 27 June
z_int_h27=[0.04693 0.06179 0.06407 0.06864];  % best
z_int_v27=[0.04807 0.06293 0.06521 0.06979]+0.004; 
h27=[10 12.4 12.6 12.8 12.9 13.1 13.2 13.5 13.7 13.8 13.8]*10^-2;
hgrid=0.065;
delta27=h27-hgrid;

% beta27=0.232;
beta27=0.1165;
L_h27=beta*delta27;
L_v27=0.155*delta27;  % This is valid in homogenous turbulence??? Kit and Fernando
L_v27=L_h27;
for n=2:4
z_star27{n}=(z_int_h27(n)-z{n})./L_h27(n);
z_starV27{n}=(z_int_v27(n)-z{n})./L_v27(n);
end

% Plot energy shifted 

figure
hold on
for n=1:3

plot(q2_ss{1,n},z_star28{n},col{n},'linestyle','--','linewidth',2)
%plot(flux_28{1,n}./q2w_o{1,n},z_star28{n},col{n},'linestyle','--')

end
for n=2:4

plot(q2_ss27{1,n},z_star27{n},col{n},'linewidth',2)
% plot(flux_27{1,n}./q2w_o27{1,n},z_star27{n},col{n})

end
ylim([-5 8])
ylabel('\xi/L_{t}')
xlabel('q''^2 [m^2/s^2]')
set(gca,'Fontsize',16)
% set(gca,'ydir','reverse')
box on

legend('1.45 hr - 28 August','3.17 hr - 28 August','4.30 hr - 28 August','2.15 hr - 27 June','3 hr - 27 June','4 hr - 27 June')

%% Plot the urms/wrms in both cases (try even in normallised axis if it nicer)


figure
hold on
for n=1:3

plot(u28ugust_nomr{1,n},z_star28{n},col{n},'linestyle','--','linewidth',2)
% plot(u28ugust_nomr{1,n},z_2{n},col{n},'linestyle','--','linewidth',2)

end
for n=2:4

plot(u27June_nomr{1,n},z_star27{n},col{n},'linewidth',2)
% plot(u27June_nomr{1,n},z{n},col{n},'linewidth',2)

end
ylim([-1 8])
ylabel('\xi/L_{t}')
% xlabel('q''^2 [m^2/s^2]')
set(gca,'Fontsize',16)
% set(gca,'ydir','reverse')
box on

legend('1.45 hr - 28 August','3.17 hr - 28 August','4.30 hr - 28 August','2.15 hr - 27 June','3 hr - 27 June','4 hr - 27 June')




