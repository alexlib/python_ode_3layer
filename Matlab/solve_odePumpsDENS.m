function [t, h, rho_1,he,rho_e] = solve_odePumpsDENS(rho_ref,w0, w2, rho2,rho1_start, rho10, h0, K, Tsim, hgrid, beta, n, A)


alpha=0.1;
% alpha=0;
he0=alpha*h0;
% rho_e0=(rho10+rho2)*0.5;
rho_e0=(rho1_start+rho2)*0.5;            % density of the top layer

% Htot=0.3;
% h20=Htot-(h0+he0);


f0 = [h0; rho1_start; he0; rho_e0;];

 % Nsteps=1000; % ode45 can choose the step size automatically
 [t, f] = ode45(@odeDENS, [0,Tsim],f0,[],rho_ref,w0, w2, rho2, rho10, K, hgrid, beta, n, A);

  h     = f(:, 1);
  rho_1 = f(:, 2);
  he    = f(:, 3);
  rho_e = f(:, 4);
  
return