clc
clear all
close all

% load the velocity field of one PIV measurement point
load 'Vel_fieldComp_400-600.mat'

% Parameters
dt=0.025;
M=(2.5*10^-2)/350;           % from px to m
Delta_frames=2;

n_frames=200;
time_vector=linspace(0,Delta_frames*200,200);

% select the point
nx=100;              % this fixs the row 
ny=80;              % this changes the column

% check where is the point that I am taking
figure
hold on
contour(Vel_field.x,Vel_field.y,Vel_field.u(:,:,100),100)
plot(Vel_field.x(nx,ny),Vel_field.y(nx,ny),'ko')

% Take a row of the velocity field
[m,n,c]=size(Vel_field.uf);

 for j=1:ny

u_row(j,1:200)=Vel_field.uf(nx,j,1:200)*(M/dt);
[xfft{1,j},magn{1,j},Energy_spectra{1,j}]=spectrum(u_row(j,1:200));

Energy_cell(:,j)=Energy_spectra{1,j};
end

    
% Average the results (average of all the spectra) of one row
Average_EnergySpect=mean(Energy_cell');

figure
plot(time_vector,u_row,'b')
xlabel('time [s]')
ylabel('uf [m/s]')
grid on
set(gca,'fontsize',14)

% figure
% plot(xfft,magn)
% xlabel('Frequency [Hz]')
% ylabel('Amplitude')
% set(gca,'fontsize',14)

% figure
% plot(xfft,Energy_spectra)
% xlabel('Frequency [Hz]')
% ylabel('E_{uu} [m2/s]')
% set(gca,'yscale','log','xscale','log')
% set(gca,'fontsize',14)

% figure
% plot(freq,abs(fftshift(xdft)));


figure
hold on
plot(xfft{1,1},Energy_cell)
plot(xfft{1,1},Average_EnergySpect,'r','linewidth',3)
xlabel('Frequency [Hz]')
ylabel('E_{uu} [m2/s]')
set(gca,'yscale','log','xscale','log')
set(gca,'fontsize',14)


