clc 
clear all
close all

density_manually=false;
% load ../Data/Exp_9March_2016.mat     % N5       % beta=0.05;     q1=0 ml/min    q2=0 ml/min       (Not entirely sure about this test)
% Data.rho10=0.9986*1000;

%  load ../Data/Exp_30March_2016_partA.mat  % N6a   % beta=0.05;     q1=0 ml/min    q2=0 ml/min
% load ../Data/Exp_30March_2016_partB.mat  % N6b   % beta=0.05;     q1=87 ml/min   q2=6.5 ml/min
%  load ../Data/Exp_28August_2016.mat       % N1    % beta=0.245;     q1=40 ml/min   q2=40 ml/min
%  load ../Data/Exp_2March_2016.mat       % N4      q1=76 ml/min   q2=6.5 ml/min

% In the following experiments we used the conductivity probe 
load ../Data/Exp_19June_2016.mat      % N2       q1=40 ml/min   q2=40 ml/min
Data.rho10=995.5;
 
%  load ../Data/Exp_27June_2016.mat      % N3       q1=40 ml/min   q2=40 ml/min
% Data.rho10=995.1;


%% IF WE WANT TO SIMULATE A FUTURE EXP

% % Define the data structure 
% % 
% Data.time=3.5*3600;
% Data.time=8*3600;  % Total time of simulation (seconds)
% Data.h=0.115;            % Initial position of the interface (m)
% 
%  Data.Q1=0/60 *10^-6 ;    %L/s
%   Data.Q2=0/60 *10^-6 ;    %L/s

% Data.rho10=0.982*1000;  % kg/m3 
% Data.rho2=1.014*1000;  % kg/m3
% 
% Data.hgrid=0.065;      % measured from the free-surface level 


%% Calculate the density
[rho1]=conductivity_to_density(Data.Cond);

if  density_manually
    Data.rho1 = Data.rho1_meas;
else   
 Data.rho1=1000*rho1;
 Data.rho1_meas=Data.rho1;
end


%% Entrainment coefficients
n = 1.5;
A = 3.8;

%% Set the Grid parameters
f = 4;
M = 0.025;
St = 0.02;

%% Grid Action Model  (K)
C = 0.181;
K = grid_action(St,M,C,f);
n_decay = 1;

%% Integral scale model (Lt=beta*z)
[Lt,~] = integral_scale(Data.Q2,Data.h-Data.hgrid); % ~ replaces beta
beta = 0.11694; % <-- why there's beta here if it is returned by the model?
% beta=0.07
%% Run the model with sensitivity
g=9.81;

% Define the buoyancy. Choose b2=0
b2 = Data.g*(Data.rho2 - Data.rho2)/Data.rho2;
b0 = Data.g*(Data.rho2 - Data.rho10)/Data.rho2;

% Simulation time
Tsim = Data.time(end)*2; 
w0 = Data.Q1/Data.S;
w2 = Data.Q2/Data.S;

% w0 = 0; % let's try without pumps
%w2 = 0;


%% SOLVE the ODE system in terms of buoyancy 

%% Coupled terms b1h1 in the derivatives  
% [t_num, h_num, b1_num,he,be] = solve_odePumps(w0, w2, b2, b0, Data.h(1), K, Tsim, Data.hgrid, beta, n, A);

%% Not-coupled terms b1h1 in the derivatives  
[t_num, h_num, b1_num,he,be] = solve_odePumpsMOD(w0, w2, b2, b0, Data.h(1), K, Tsim, Data.hgrid, beta, n, A);


[Ri_num,E_num,we_num,db_num] = entrainment_num(b1_num,b2,h_num,Data.hgrid,beta,K,A,n,n_decay);
rho_e=Data.rho2*(1-be./g);
rho1_num=Data.rho2*(1-b1_num./g);


%% 
% c=7e-6;
% delta=c*h_num.*Ri_num';

h2 = 0.3 - (h_num + he); % 0.3 = H = total liquid height

figure
subplot(121)
hold on
% plot(t_num/3600,delta,'k')
plot(t_num/3600,h_num,'b')
plot(t_num/3600,he,'r')
plot(t_num/3600,h2,'k')
xlabel('t [hr]')
ylabel('h,he,h2 [m]')
set(gca,'fontsize',14)
legend('h_1','h_e','h_2')

%Alex:
hold on
plot(Data.time/3600,Data.h,'o')


subplot(122)
hold on
% plot(t_num/3600,delta,'k')
plot(t_num/3600,rho1_num,'b')
plot(t_num/3600,rho_e,'r')
xlabel('t [hr]')
ylabel('rho_e [kg/m3]')
set(gca,'fontsize',14)
legend('\rho_1','\rho_e')

% Alex:
hold on
plot(Data.time/3600,Data.rho1,'o')


%% Sketch of the system 
drho_dhe=(rho_e-rho1_num)./he;

figure
plot(t_num/3600,drho_dhe)
xlabel('t [hr]')
ylabel('db/dz [1/s2]')
set(gca,'fontsize',14)

% System sketch
H_tot=0.3;     % h_total
h1_s=ones(1,10)*h_num(end);
he_s=ones(1,10)*he(end);

% density of the top layer
rho_1y=linspace(0,h1_s(end),20);
rho_1p=ones(1,20).*rho1_num(end);
% density of the bottom layer
rho_2y=linspace(he(end)+h1_s(end),H_tot,20);
rho_2p=ones(1,20).*Data.rho2(end);
% Draw the density in the interfacial layer
yrho_int=linspace(h1_s(end),h1_s(end)+he_s(end),20);
rho_inter=ones(1,20)*rho_e(end);

% create the frame
x=linspace(0,0.2,10);
z=linspace(0,0.3,20);

% Start plotting the steady state conditions
% figure
% subplot(121)
% rectangle('Position',[0 0 0.2 0.3])
% axis([0 0.2 0 0.3])
% set(gca,'ydir','reverse')
% hold on
% plot(x,h1_s,'b')
% plot(x,(he_s+h1_s),'r')
% set(gca,'fontsize',14)

figure
hold on
plot(rho_1p,rho_1y,'b')
plot(rho_2p,rho_2y,'k')
% plot(rho_inter,yrho_int,'r')
plot([rho_1p(end) rho_2p(end)],[rho_1y(end) rho_2y(1)],'r')
plot([1000 1200],[rho_1y(end) rho_1y(end)],'b','linestyle','--')
plot([1000 1200],[rho_2y(1) rho_2y(1)],'r','linestyle','--')
set(gca,'ydir','reverse')
set(gca,'fontsize',14)
ylim([0 0.3])
xlim([1010 1060])
ylabel('z [m]')
xlabel('\rho [kg/m3]')
box on


clear h1_S he_s rho_1y rho_1p rho_2y rho_2p yrho_int
%%  

H_tot=0.3;     % h_total
cm=colormap(parula(length(h_num)));

figure
for n=1:50:length(h_num)
    
h1_s=ones(1,10)*h_num(n);
he_s=ones(1,10)*he(n);

% density of the top layer
rho_1y=linspace(0,h1_s(1),20);
rho_1p=ones(1,20).*rho1_num(n);
% density of the bottom layer
rho_2y=linspace(he_s(end)+h1_s(end),H_tot,20);
rho_2p=ones(1,20).*Data.rho2;
% Draw the density in the interfacial layer
yrho_int=linspace(h1_s(end),h1_s(end)+he_s(end),20);
% rho_inter=ones(1,20)*rho_e(end);


hold on
plot(rho_1p,rho_1y,'color',cm(n,:))
plot(rho_2p,rho_2y,'k')
plot([rho_1p(end) rho_2p(end)],[rho_1y(end) rho_2y(1)],'color',cm(n,:))
% plot([1000 1200],[rho_1y(end) rho_1y(end)],'b','linestyle','--')
% plot([1000 1200],[rho_2y(1) rho_2y(1)],'r','linestyle','--')
end

set(gca,'ydir','reverse')
set(gca,'fontsize',14)
ylim([0 0.3])
xlim([(Data.rho10-10) (Data.rho2+10)])
ylabel('z [m]')
xlabel('\rho [kg/m3]')
box on

%% Sensitivity analysis


% Delta_h_it=linspace(0,0.005,length(h));      % vector of errors h from 0 to 1 cm;
Delta_h_it=0.005*ones(1,length(h_num)); 
Delta_b_it=0.03*ones(1,length(h_num));     % vector of errors db from 0 to 1 m2/s;
Delta_hgrid_it=0.005*ones(1,length(h_num)); 
Delta_w2_it=((4/60 *10^-6)/Data.S) *ones(1,length(h_num));

rho1_num=Data.rho2 * (1 - b1_num/Data.g);
rho1_num_plusdrho=Data.rho2 * (1 -(b1_num+Delta_b_it')/Data.g);
rho1_num_mindrho=Data.rho2 * (1 -(b1_num-Delta_b_it')/Data.g);


% sensitivity(Delta_h,Delta_hgrid,Delta_w2,Delta_beta,Delta_C,Delta_db,h,db,C,beta,S,M,f,hgrid,w2)

% First variable (h)
for i=1:length(h_num);
[dRi_total_h(i), dE_total_h(i)]=sensitivity(Delta_h_it(i),Delta_hgrid_it(i),Delta_w2_it(i),0,0,Delta_b_it(i),h_num(i),db_num(i),C,beta,Data.S,M,f,Data.hgrid,w2);
end


%% Add errorbar

figure
subplot(221)
hold all

plot(t_num/3600, h_num,'-b','linewidth',2)
plot(t_num/3600, h_num+Delta_h_it','--k','linewidth',2)
plot(t_num/3600, h_num-Delta_h_it','--r','linewidth',2)
% plot(t_num/3600+3.5, h_num,'-b','linewidth',2)
% plot(t_num/3600+3.5, h_num+Delta_h_it','--k','linewidth',2)
% plot(t_num/3600+3.5, h_num-Delta_h_it','--r','linewidth',2)
xlabel('t [hr]')
ylabel('h [m]')
box on;

subplot(222)
hold all
 plot(t_num/3600,rho1_num ,'-b','linewidth',2)
 plot(t_num/3600, rho1_num_plusdrho ,'--k','linewidth',2)
 plot(t_num/3600, rho1_num_mindrho ,'--r','linewidth',2)
%  plot(t_num/3600+3.5,rho1_num ,'-b','linewidth',2)
%  plot(t_num/3600+3.5, rho1_num_plusdrho ,'--k','linewidth',2)
%  plot(t_num/3600+3.5, rho1_num_mindrho ,'--r','linewidth',2)
xlabel('t [hr]')
ylabel('\rho_{1} [kg/m3]')
box on

subplot(223)
hold all
% plot(t_num/3600+3.5, Ri_num ,'-b','linewidth',2)
% plot(t_num/3600+3.5, Ri_num+dRi_total_h ,'--k','linewidth',2)
% plot(t_num/3600+3.5, Ri_num-dRi_total_h ,'--r','linewidth',2)
plot(t_num/3600, Ri_num ,'-b','linewidth',2)
plot(t_num/3600, Ri_num+dRi_total_h ,'--k','linewidth',2)
plot(t_num/3600, Ri_num-dRi_total_h ,'--r','linewidth',2)

xlabel('t [hr]')
ylabel('Ri ')
box on;

subplot(224)
hold all
% plot(t_num/3600+3.5, E_num,'-b','linewidth',2)
% plot(t_num/3600+3.5, E_num+dE_total_h,'--k','linewidth',2)
% plot(t_num/3600+3.5, E_num-dE_total_h,'--r','linewidth',2)
plot(t_num/3600, E_num,'-b','linewidth',2)
plot(t_num/3600, E_num+dE_total_h,'--k','linewidth',2)
plot(t_num/3600, E_num-dE_total_h,'--r','linewidth',2)

xlabel('t [hr]')
ylabel('E')
box on


figure
subplot(1,2,1)
hold all
plot(t_num/3600, h_num,'--b','linewidth',3)
xlabel('t [hr]')
ylabel('h [m]')
box on;

subplot(1,2,2)
hold all
plot(t_num/3600,rho1_num/1000 ,'--r','linewidth',3)
xlabel('t [hr]')
ylabel('\rho_{3} [kg/m3]')
% ylim([-0.15 0])

%% 

figure
subplot(211)
hold all
plot(t_num/3600, Ri_num,'--k','linewidth',3)
xlabel('t [hr]')
ylabel('Ri ')
box on;

subplot(212)
hold all
plot(t_num/3600, E_num,'--r','linewidth',3)
xlabel('t [hr]')
ylabel('E')
box on



%% Phase portraits

% w0=w1;

% Buoyancy flux 
Bexp=(w0+w2)/w2;
b1f=b1_num(end);    

delta_ss=((K^(1+2*n)*A)/(w2*b0^n*beta^n))^(1/(3*n+1)) * ((w0+w2)/w0)^(n/(3*n+1));
d_dim=(h_num - Data.hgrid)./(h_num(end)- Data.hgrid);
% d_dim=(h_num - Data.hgrid)./(delta_ss- Data.hgrid);

b_dim=b1_num./b1f;


figure
hold on
    phaseplotnd(Bexp, Data.hgrid);
    plot(d_dim,b_dim,'b-.','linewidth',2)
    title(['B = ', num2str(Bexp, '%8.1f')])
% xlabel('\hat{d}')
ylabel('$$\hat{b1}$$','Interpreter','Latex')
xlabel('$$\hat{d}$$','Interpreter','Latex')
set(gca,'fontsize',14)
%% function phaseplotnd

type phaseplotnd

%% function box1nd

type box1nd


