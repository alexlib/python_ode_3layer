clear 
clc
close all 

load ../Data/Exp_19June_2016.mat      % N2       q1=40 ml/min   q2=40 ml/min

%% Plot urms integrated with a constant buoacy flux imposed

S=0.02;
f=4;
x0=0.01; %m
u0=S*f;
beta=0.1165;
r=2.2;
A=3*beta*r;
hss=0.14;   % estimate 13 cm 
hgrid=0.065;
D=hss-hgrid;
Q2=Data.Q2;
Q0=Data.Q1;

Area=0.02;
g=9.81;

u00=Q0/Area;  % m/s
u2=Q2/Area;   % m/s

b0=g*(Data.rho2 - Data.rho10)/Data.rho2;
b2=0;

q0=u00*b0;   % m2/s3
q2=u2*b2;

%% I need to convert these values from L/s in   

x=linspace(0,D,100);

% urms_zPump=u0*(z./z0).^(-A/(3*r*beta))  + u0*exp((q0/(3*r))*(z-z0));
% urms_zPump=u0*(z./z0).^(-A/(3*r*beta))  + u0*exp((q0/(6*r*h))*(z-z0).^2);
urms_z=u0*(x./x0).^(-A/(3*r*beta));
%% Integration of the energy eq. with q0=w0b0*(z-D)/D  and q2=const

urms_z2Pump=1/(D*(2*beta^2*r^2+3*A*beta*r+A^2))*(2*x.^(-(1/3)*A/(beta*r))*x0^((1/3)*A/(beta*r))*beta^2*r^2*u0*D+3*x.^(-(1/3)*A/(beta*r))*x0^((1/3)*A/(beta*r))*A*beta*r*u0*D ...
    +2*x.^(-1/3*(A)/(beta*r))*x0^((beta*r+A)/(beta*r))*beta^2*q0*r*D-2*x.^(-1/3*(A)/(beta*r))*x0^((beta*r+A)/(beta*r))*beta^2*q2*r*D ...
    +x.^(-1/3*(A)/(beta*r))*x0^(1/3*(A)/(beta*r))*A^2*u0*D+x.^(-1/3*(A)/(beta*r))*x0^((beta*r+A)/(beta*r))*A*beta*q0*D-x.^(-1/3*(A)/(beta*r))*x0^((beta*r+A)/(beta*r))*A*beta*q2*D...
    -2*x.^((1/3)*(3*beta*r+2*A)/(beta*r))*beta^2*q0*r*D+2*x.^((1/3)*(3*beta*r+2*A)/(beta*r))*beta^2*q2*r*D-x.^(-(1/3)*A/(beta*r))*x0^((2*beta*r+A)/(beta*r))*beta^2*q0*r...
    -x.^((1/3)*(3*beta*r+2*A)/(beta*r))*beta*A*q0*D+x.^((1/3)*(3*beta*r+2*A)/(beta*r))*beta*A*q2*D-x.^(-(1/3)*A/(beta*r))*x0^((2*beta*r+A)/(beta*r))*A*beta*q0...
    +x.^(2/3*(3*beta*r+A)/(beta*r))*beta^2*q0*r+x.^(2/3*(3*beta*r+A)/(beta*r))*beta*A*q0);

% Simplified Hopfinger&Linden 1982
urms_hop=u0.*(x0./x).*(1-((q0*x.^4)./(r*D*(u0*x0)^3)).*(D/4 - x./5)).^(1/3);

figure
hold on
plot(urms_z,x,'b-')
plot(urms_z2Pump,x,'r-')
plot(urms_hop,x,'k-')

set(gca,'fontsize',14)
set(gca,'ydir','reverse')
ylim([0.02 D])
ylabel('z')
xlabel('u_{rms}')