function dfdt = ode(t,f, w0, w2, b2, b10 , K, hgrid, beta, n, A);

% unpack vector
h=f(1); b1h=f(2); he=f(3); behe=f(4);% h2=f(5); b2h2=f(6); 

% h or he cannot be negative
if he <= 0, he = 1e-3; end
if h  <= 0, h = 1e-3; end
    
% here the simulation stops when h goes to zero, so it's nonsense
b1 = b1h/h;  be = behe/he; %b2=b2h2/h2; 

% be=(b1)*0.5;

db = abs(b2 - b1);
w_star=K./(h-hgrid).^(1);
Lt=beta*(h-hgrid);
Ri=(db.*Lt)./w_star.^2;
% Ri=(db.*he)./w_star.^2;


% I fit n and A from exp and Hopfinger
E=A*Ri.^(-n);
we=E.*w_star;


% ODE system (Complete equation - original)
% dhdt = we - w2;
% db1hdt = we * b2 + w0 * b10 - (w0+w2)*b1;


% System adapted to the three layers case

% % TOP LAYER
% dhdt = we - w2;
% db1hdt = we * (be-b1) + w0 * b10 - (w0+w2)*b1;
% 
% % INTERFACIAL LAYER
% dhedt= we - w2;
% dbehedt= w2*b2 + we*(b1 -be);             
%                                            
% % BOTTOM LAYER
% dh2dt= - dhdt - dhedt;
% db2h2dt=w2*b2 - we*b2;

% If there is no turbulence active we=w2 at the interface

% we=w2;
%% 

% % top layer 
dhdt= we - w2;
db1hdt= b10*w0 + be*we - b1*(w0+w2);

% % % Bottom layer
% dh2dt= w2-we;
% db2h2dt= w2*b2 - we*b2;

% mid layer
% dhedt= -dhdt - dh2dt;
dhedt= w2-we;
dbehedt = b2*w2 - be*we;


% % 



dfdt = [dhdt; db1hdt; dhedt; dbehedt];

% pack vector
% dfdt = [dhdt; db1hdt; dhedt; dbehedt; dh2dt; db2h2dt];
