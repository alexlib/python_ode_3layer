% Urms 27 June and 28 August compared 

clc
clear all
close all

% Add ezyfit
addpath(genpath('../../Data/ezyfit'));

tke_27June=load('TKE_27June2016.mat');
tke_28Aug=load('TKE_28August2016.mat');

diss_27June=load('Diss_27June2016.mat');
diss_28Aug=load('Diss_28August2016.mat');

% Conversion 
dt=0.025;    % seconds  25000 micros
M=(2.5*10^-2)/350;           % from px to m


%% Test 27 June 2016
dy_27June=510*M; 
dy_28Aust=80*M; 


for n=1:3
z27{n}=flipud(tke_27June.tke_27{1,n}(:,2)*M) - dy_27June;
z28{n}=flipud(tke_28Aug.TKE_28Aug{1,n}(:,2)*M) + dy_28Aust;

% Tke:rename and convert the object
tke27{1,n}=tke_27June.tke_27{1,n}(:,1)*((M/dt)^2);
tke28{1,n}=tke_28Aug.TKE_28Aug{1,n}(:,1)*((M/dt)^2);

% Dissipation:rename and convert the object
Diss27{1,1}=diss_27June.Diss_27{1,1}(1:104,1)*((M^2/dt));
Diss27{1,2}=diss_27June.Diss_27{1,2}(1:103,1)*((M^2/dt));
Diss27{1,3}=diss_27June.Diss_27{1,3}(1:104,1)*((M^2/dt));

Diss28{1,n}=diss_28Aug.Diss_28Aug{1,n}(1:105,1)*((M^2/dt));

end


figure
subplot(121)
hold on
for n=1:3
hold on
plot(tke27{1,n},z27{n},'linewidth',2)
end
for n=1:3
plot(tke28{1,n},z28{n},'linewidth',2,'linestyle','-.')
end
ylabel('z [m]')
xlabel('tke [m2/s2]')
set(gca,'Fontsize',16)
set(gca,'ydir','reverse')
box on
legend('2.15 hr - 27June','3 hr - 27June','4 hr - 27June','1.47 hr - 28August','3.17 hr - 28August','4.30 hr - 28August')
title('tke')
ylim([0.02 0.1])


subplot(122)
hold on
for n=1:3
hold on
plot(Diss27{1,n},z27{n},'linewidth',2)
end
for n=1:3
plot(Diss28{1,n},z28{n},'linewidth',2,'linestyle','-.')
end
ylabel('z [m]')
xlabel('\epsilon [m2/s3]')
set(gca,'Fontsize',16)
set(gca,'ydir','reverse')
box on
legend('2.15 hr - 27June','3 hr - 27June','4 hr - 27June','1.47 hr - 28August','3.17 hr - 28August','4.30 hr - 28August')
title('\epsilon')
ylim([0.02 0.1])


%% Create a shifted plot in reference to the interface position
% 28 August
z_int_h28=[0.07307 0.07536 0.0787];  % best
z_int_v28=z_int_h28; 
h=[0.115000000000000,0.135000000000000,0.140000000000000,0.145000000000000,0.145000000000000,0.145000000000000];
hgrid=0.065;
delta28=h-hgrid;

% 27 June
z_int_h27=[0.04693 0.06179 0.06407 0.06864];  % best
z_int_v27=[0.04807 0.06293 0.06521 0.06979]+0.004; 
h27=[10 12.4 12.6 12.8 12.9 13.1 13.2 13.5 13.7 13.8 13.8]*10^-2;
hgrid=0.065;
delta27=h27-hgrid;

beta=0.1165;
L_h28=beta*delta28;
L_v28=0.155*delta28;  % This is valid in homogenous turbulence??? Kit and Fernando
L_v28=L_h28;

% beta27=0.232;
beta27=0.1165;
L_h27=beta*delta27;
L_v27=0.155*delta27;  % This is valid in homogenous turbulence??? Kit and Fernando
L_v27=L_h27;


for n=1:3
z_star28{n}=(z_int_h28(n)-z28{n})./L_h28(n);
z_starV28{n}=(z_int_v28(n)-z28{n})./L_v28(n);
z_star27{n}=(z_int_h27(n)-z27{n})./L_h27(n);
z_starV27{n}=(z_int_v27(n)-z27{n})./L_v27(n);

end




%% Plot energy shifted 

figure
subplot(121)
hold on
for n=1:3

plot(tke27{1,n},z_star27{n},'linewidth',2)

end
for n=1:3

plot(tke28{1,n},z_star28{1,n},'linestyle','--','linewidth',2)

end

ylim([-5 6])
ylabel('\xi/L_{t}')
xlabel('q''^2 [m^2/s^2]')
set(gca,'Fontsize',16)
% set(gca,'ydir','reverse')
box on
legend('2.15 hr - 27June','3 hr - 27June','4 hr - 27June','1.47 hr - 28August','3.17 hr - 28August','4.30 hr - 28August')

subplot(122)
hold on

for n=1:3

plot(Diss27{1,n},z_star27{n},'linewidth',2)

end
for n=1:3

plot(Diss28{1,n},z_star28{n},'linestyle','--','linewidth',2)

end
ylim([-5 6])
ylabel('\xi/L_{t}')
xlabel('\epsilon [m^2/s^3]')
set(gca,'Fontsize',16)
% set(gca,'ydir','reverse')
box on
legend('2.15 hr - 27June','3 hr - 27June','4 hr - 27June','1.47 hr - 28August','3.17 hr - 28August','4.30 hr - 28August')

%% LOad urms and try to calculate the constant A

Urms_27June=load('../../Data/2layers_27June16/Exp_27June/PIV_27June_ENTIRE/RMS_27June.mat'); 
Urms_28August=load('../../Data/2layers_28August16/RMS_28August16.mat');

for n=2:4

sigma_u27cub{1,n-1}=(Urms_27June.RMS{1,n}.u).^3;
end
for n=1:3
sigma_u28cub{1,n}=Urms_28August.RMS{1,n}.u.^3;
end

figure
subplot(121)
hold on
for n=1:3

plot((Diss27{1,n}(1:103,1))./(sigma_u27cub{1,n}(1,1:103)'),z27{n}(1:103,1),'linewidth',2)

end

ylabel('z [m]')
xlabel('\epsilon/\sigma^3')
set(gca,'Fontsize',16)
set(gca,'ydir','reverse')
ylim([0 0.06])

subplot(122)
hold on
for n=1:3

plot((Diss28{1,n})./(sigma_u28cub{1,n}),z28{n},'linewidth',2)

end

ylabel('z [m]')
xlabel('\epsilon/\sigma^3')
set(gca,'Fontsize',16)
set(gca,'ydir','reverse')
ylim([0 0.06])


